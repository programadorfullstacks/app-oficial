import 'package:flutter/material.dart';
import 'package:playmedia/tabs/first.dart';
import 'package:playmedia/tabs/second.dart';
import 'package:playmedia/tabs/third.dart';
import 'package:playmedia/tabs/fourth.dart';
import 'package:playmedia/tabs/country.dart';

class MyHome extends StatefulWidget {
  @override
  MyHomeState createState() => MyHomeState();
}

// SingleTickerProviderStateMixin is used for animation
class MyHomeState extends State<MyHome> with SingleTickerProviderStateMixin {
  // Create a tab controller
  TabController controller;

  @override
  void initState() {
    super.initState();

    // Initialize the Tab Controller
    controller = TabController(length: 5, vsync: this);
  }

  @override
  void dispose() {
    // Dispose of the Tab Controller
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Appbar

      // Set the TabBar view as the body of the Scaffold
      body: TabBarView(
        // Add tabs as widgets
        children: <Widget>[
          FirstTab(),
          SecondTab(),
          ThirdTab(),
          Fourthtabs(),
          Countrytabs(),
        ],
        // set the controller
        controller: controller,
      ),
      // Set the bottom navigation bar
      bottomNavigationBar: Material(
        // set the color of the bottom navigation bar
        color: Colors.blue[900],
        // set the tab bar as the child of bottom navigation bar
        child: TabBar(
          tabs: <Tab>[
            Tab(
              // set icon to the tab
              icon: Icon(Icons.home),
              text: 'Inicio',
            ),
            Tab(
              icon: Icon(Icons.business),
              child: Text(
                'Sección',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11,
                ),
              ),
            ),
            Tab(
              icon: Icon(Icons.control_point_sharp),
              child: Text(
                'Nosotros',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 9,
                ),
              ),
            ),
            Tab(
              icon: Icon(Icons.school),
              child: Text(
                'oferta educativa',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 9,
                ),
              ),
            ),
            Tab(
              icon: Icon(Icons.contact_mail_outlined),
              child: Text(
                'Contacto',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 9,
                ),
              ),
            ),
          ],
          // setup the controller
          controller: controller,
        ),
      ),
    );
  }
}
