import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

//import 'package:playmedia/personal.dart';

void main() => runApp(FirstTab());

// ignore: must_be_immutable
class FirstTab extends StatelessWidget {
  List<String> images = [
    "https://drive.google.com/uc?export=view&id=1Hn2zgosPTFcac-ybPIw5uNm1-6oMU2fQ",
    "https://drive.google.com/uc?export=view&id=16A2IXnzgWL1B8ce6hjyQO-NgdSem3P_m",
    "https://drive.google.com/uc?export=view&id=1ODpCwzeLW0UVRqqkzbai6YeHIX7WMT_E"
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blue[900],
            elevation: 0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // ignore: missing_required_param
                IconButton(
                  icon: Image.network(
                      "https://drive.google.com/uc?export=view&id=1egwV-HxzGBCXR3j97iBMOtP8ikPheXpF",
                      width: 100),
                ),
                Text(
                  'TECNM CAMPUS CHINÁ',
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
              ],
            ),
          ),
          body: Container(
              margin: EdgeInsets.all(5.0),
              child: ListView(
                children: [
                  _swiper(),
                  SizedBox(
                    height: 20,
                  ),
                  ListTile(
                    // leading: Icon(Icons.photo_album, color: Colors.blue),
                    title: Text(
                      "MISIÓN",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 23, color: Colors.black),
                    ),

                    subtitle: RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                                text:
                                    'Formar profesionales humanistas con pertinencia, portadores de conocimientos vanguardistas y competitivos; emprendedores e innovadores a través de una educación superior científica y tecnológica de calidad.'),
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  ListTile(
                    // leading: Icon(Icons.photo_album, color: Colors.blue),
                    title: Text(
                      "VISIÓN",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 23, color: Colors.black),
                    ),

                    subtitle: RichText(
                      textAlign: TextAlign.justify,
                      text: TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                                text:
                                    'Ser una Institución educativa formadora de ciudadanos del mundo a través del desarrollo sostenido sustentable y equitativo. Con personal capacitado con altos estándares de calidad; protectora del medio ambiente e interactuando con las necesidades que exigen los cambios del País y la comunidad.'),
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        style: TextStyle(
                          fontSize: 20,
                          //fontFamily: 'marker',
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                        children: [
                          TextSpan(text: 'Directivos'),
                        ]),
                  ),
                  Image.network('https://i.postimg.cc/2yXhPvQx/Directivos.jpg'),
                  new Padding(padding: new EdgeInsets.all(2.00)),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        style: TextStyle(
                          fontSize: 20,
                          //fontFamily: 'marker',
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                        children: [
                          TextSpan(text: 'Docentes'),
                        ]),
                  ),
                  Image.network('https://i.postimg.cc/CMvGBy3f/Docentes2.jpg'),
                ],
              ))),
    );
  }

  Widget _swiper() {
    return Container(
      width: double.infinity,
      height: 150.0,
      child: Swiper(
        viewportFraction: 0.9,
        scale: 0.9,
        itemBuilder: (BuildContext context, int index) {
          return new Image.network(
            images[index],
            fit: BoxFit.fill,
          );
        },
        itemCount: 3,
        control: new SwiperControl(color: Colors.blue[900]),
      ),
    );
  }
}
