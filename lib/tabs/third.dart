import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'munulateral/organigrama.dart';
import 'munulateral/mensajedeldirector.dart';
import 'munulateral/historia.dart';
import 'munulateral/directoriomaestros.dart';
import 'munulateral/normateca.dart';

class ThirdTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: [
      Container(
          child: Column(
        children: [
          SizedBox(
              height: 50,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [],
              )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            height: MediaQuery.of(context).size.height - 200,
            child: Column(children: [
              mainCard(context),
              SizedBox(height: 40),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      child: regularCard(
                          'assets/mensaje.svg', 'Mensaje\n del director'),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Mensaje()),
                        );
                      },
                    ),
                    GestureDetector(
                      child: regularCard('assets/historia.svg', 'Historia'),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Historia()),
                        );
                      },
                    ),
                    GestureDetector(
                      child: regularCard('assets/directorio.svg', 'Directorio'),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Directorio()),
                        );
                      },
                    ),
                  ]),
              SizedBox(height: 20),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      child: regularCard('assets/filosofia.svg', 'Filosofia'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child:
                          regularCard('assets/organigram.svg', 'Organigrama'),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PDF()),
                        );
                      },
                    ),
                    GestureDetector(
                      child: regularCard('assets/normateca.svg', 'Normateca'),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Normateca()),
                        );
                      },
                    ),
                  ])
            ]),
          ),
        ],
      )),
    ]));
  }

  SizedBox regularCard(String iconName, String cardLabel) {
    return SizedBox(
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey[100],
          ),
          child: SvgPicture.asset(iconName,
              width:
                  40), //se utiliza puras imagenes svg y que esten guardatos en la carpeta no he buscado como traerlo de la web
        ),
        SizedBox(height: 2),
        Text(cardLabel,
            textAlign: TextAlign.center,
            style: textStyle(16, FontWeight.w600, Colors.black))
      ]),
    );
  }

// todo estos widget son d ela carta azul//
  Container mainCard(context) {
    return Container(
        padding: EdgeInsets.only(left: 20, right: 20, top: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey[300], offset: Offset.zero, blurRadius: 20)
          ],
        ),
        child: Row(children: [
          Container(
            alignment: Alignment.bottomCenter,
            width: (MediaQuery.of(context).size.width - 80) / 2,
            height: 70,
            child: Image.asset(
              "assets/itchinalogo.png",
            ),
          ),
          SizedBox(
            width: (MediaQuery.of(context).size.width - 80) / 2,
            height: 150,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text('SOMOS TECNM CAMPUS CHINÁ',
                  style: textStyle(15, FontWeight.w500, Colors.black)),
              SizedBox(height: 16),
              Text('EXCELENCIA EN EDUCACIÓN TECNOLÓGICA',
                  style: textStyle(15, FontWeight.w500, Colors.black))
            ]),
          ),
        ]));
  }

  TextStyle textStyle(double size, FontWeight fontWeight, Color colorName) =>
      TextStyle(
        color: colorName,
        fontSize: size,
        fontWeight: fontWeight,
      );
}
// todo estos widget son d ela carta azul//
