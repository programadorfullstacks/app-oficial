import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

/// Represents Homepage for Navigation

/// Represents Homepage for Navigation
class Normateca extends StatefulWidget {
  @override
  _NormatecaPage createState() => _NormatecaPage();
}

class _NormatecaPage extends State<Normateca> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Normateca'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.bookmark,
              color: Colors.white,
            ),
            onPressed: () {
              _pdfViewerKey.currentState?.openBookmarkView();
            },
          ),
        ],
      ),
      body: SfPdfViewer.network(
        'https://drive.google.com/uc?export=view&id=1WnvgOJ_1zdAGGTbZQFscrkMFlD_BiDBJ',
        key: _pdfViewerKey,
      ),
    );
  }
}
