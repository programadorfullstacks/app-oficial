import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

/// Represents Homepage for Navigation

/// Represents Homepage for Navigation
class Directorio extends StatefulWidget {
  @override
  _DirectorioPage createState() => _DirectorioPage();
}

class _DirectorioPage extends State<Directorio> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Directorio'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.bookmark,
              color: Colors.white,
            ),
            onPressed: () {
              _pdfViewerKey.currentState?.openBookmarkView();
            },
          ),
        ],
      ),
      body: SfPdfViewer.network(
        'https://drive.google.com/uc?export=view&id=1_f0V9kXWe5XIr8kJEnTTPwwmRlgmq1v7',
        key: _pdfViewerKey,
      ),
    );
  }
}
