import 'package:flutter/material.dart';

import 'package:getwidget/getwidget.dart';

void main() {
  runApp(MaterialApp(home: Mensaje()));
}

class Mensaje extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mensaje'),
      ),
      body: ListView(
        children: <Widget>[
          GFAccordion(
              collapsedIcon: Icon(Icons.add),
              expandedIcon: Icon(Icons.minimize),
              title: "El Instituto Tecnológico de Chiná (ITChiná)",
              content:
                  "Ha formado a más de 2,680 profesionistas en las diferentes carreras que ha ofertado. Actualmente ofrece siete programas educativos: Ingeniería en Agronomía, Ingeniería Forestal, Ingeniería en Gestión Empresarial, Ingeniería en Administración, Licenciatura en Biología, Ingeniería Informática e Ingeniería en Industrias Alimentarias, así como las Maestrías en: Agroecosistemas Sostenibles (en el PNPC de CONACYT) y Administración."),
          GFAccordion(
              collapsedIcon: Icon(Icons.add),
              expandedIcon: Icon(Icons.minimize),
              title:
                  "El ITChiná inició a partir de abril de 2014 un proceso profundo de transformación.",
              content:
                  "Consistente en trabajar sobre el cumplimiento de indicadores de calidad. Para ello; se trabajó en una autoevaluación con fines de acreditación de todos sus programas académicos acreditables, logrando a la fecha la acreditación de todos."),
          GFAccordion(
              collapsedIcon: Icon(Icons.add),
              expandedIcon: Icon(Icons.minimize),
              title:
                  "Para atender los servicios ofertados, el ITChiná cuenta con los siguientes recursos humanos:",
              content:
                  "66 Profesores y 27 de apoyo y asistencia a la educación. del total de profesores, 9 cuentan con doctorado y 36 con maestría; De los cuales 7 están en el Sistema Nacional de Investigadores (SNI) y 10 cuentan con el reconocimiento de Perfil Deseable del PROMEP actualmente se encuentran operando 3 cuerpos académicos: Investigación Agropecuaria y Forestal para el Desarrollo Sustentable del Trópico, Agroecología y Desarrollo Sustentable y Agroecosistemas y Conservación de la Biodiversidad.")
        ],
      ),
    );
  }
}
