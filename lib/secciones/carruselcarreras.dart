import 'package:flutter/material.dart';
import 'agronomia.dart';
import 'forestal.dart';

class Carruselcarreras extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<Carruselcarreras> {
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    var size = MediaQuery.of(context).size;
    // ignore: unused_local_variable
    var cardTextStyle = TextStyle(fontFamily: "Monserrat regular");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[900],
      ),
      body: Stack(
        children: <Widget>[
          Container(),
          SafeArea(
            child: Padding(
              padding: EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        CircleAvatar(
                          radius: 50,
                          backgroundImage: NetworkImage(
                              "https://i.pinimg.com/originals/dc/be/6e/dcbe6ee57620a94b35df7253636e5fe2.png"),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('TECNM CAMPUS CHINÁ'),
                            Text('TU MEJOR OPCIÓN')
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: GridView.count(
                      mainAxisSpacing: 15,
                      crossAxisSpacing: 15,
                      primary: false,
                      children: <Widget>[
                        GestureDetector(
                            child: Card(
                              elevation: 10,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.network(
                                    "https://www.utalca.cl/content/uploads/2021/01/Universidad-de-Talca-1.png",
                                    height: 80,
                                  ),
                                  Text(
                                    "INGENIERÍA EN AGRONOMIA",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.blue),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Agronomia()),
                              );
                            }),
                        GestureDetector(
                            child: Card(
                              elevation: 10,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.network(
                                    "https://drive.google.com/uc?export=view&id=1HGB8ic5mu9s-lUxn9BZc4WeWW4mTGCCh",
                                    height: 80,
                                  ),
                                  Text(
                                    "INGENIERÍA FORESTAL",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.blue),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Forestal()),
                              );
                            }),
                        Card(
                          elevation: 10,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.network(
                                "https://facultades.usil.edu.pe/ingenieria/wp-content/uploads/2019/07/ingenieria-industrias-alimentarias.jpg",
                                height: 80,
                              ),
                              Text(
                                "INGENIERÍA EN INDUSTRIAS ALIMENTARIAS",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 15, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          elevation: 10,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.network(
                                "https://image.freepik.com/vector-gratis/concepto-diseno-espacio-trabajo-laboratorio-biologia_1284-11559.jpg",
                                height: 80,
                              ),
                              Text(
                                "LICENCIATURA EN BIOLÓGIA",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 15, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          elevation: 10,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.network(
                                "https://img.freepik.com/foto-gratis/imagen-primer-plano-programador-trabajando-su-escritorio-oficina_1098-18707.jpg?size=338&ext=jpg",
                                height: 80,
                              ),
                              Text(
                                "INGENIERÍA INFORMÁTICA",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 15, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          elevation: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.network(
                                "https://img.freepik.com/foto-gratis/contador-calculando-ganancias-graficas-analisis-financiero_74855-4937.jpg?size=338&ext=jpg",
                                height: 80,
                              ),
                              Text(
                                "INGENIERÍA EN ADMINISTRACIÓN",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 15, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          elevation: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.network(
                                "https://image.freepik.com/vector-gratis/analisis-rendimiento-empresarial-graficos_53876-59914.jpg",
                                height: 80,
                              ),
                              Text(
                                "INGEIERÍA EN GESTION EMPRESARIAL",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 15, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          elevation: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.network(
                                "https://storage.contextoganadero.com/s3fs-public/styles/noticias_one/public/agricultura/field_image/2020-04/agroecosistema.jpg?itok=68e6DqGA",
                                height: 80,
                              ),
                              Text(
                                "MAESTRÍA EN CIENCIAS EN AGROSISTEMAS SOSTENIBLE",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 14, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                        Card(
                          elevation: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.network(
                                "https://img.freepik.com/foto-gratis/vista-aerea-grafico-analisis-datos-negocios_53876-13390.jpg?size=338&ext=jpg",
                                height: 80,
                              ),
                              Text(
                                "MAESTRÍA EN ADMINISTRACIÓN",
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(fontSize: 15, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                      ],
                      crossAxisCount: 2,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
