import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

/// Represents Homepage for Navigation

/// Represents Homepage for Navigation
class Agronomia extends StatefulWidget {
  @override
  _AgronomiaPage createState() => _AgronomiaPage();
}

class _AgronomiaPage extends State<Agronomia> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Agronomia'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.bookmark,
              color: Colors.white,
            ),
            onPressed: () {
              _pdfViewerKey.currentState?.openBookmarkView();
            },
          ),
        ],
      ),
      body: SfPdfViewer.network(
        'https://drive.google.com/uc?export=view&id=1UUsG66KRk038tYlQrMPvAGNcUCxJgJMA',
        key: _pdfViewerKey,
      ),
    );
  }
}
